import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  result = 'test hello';
  result2 = ['1', 2, true];
  result3 = [
    {
      first_name: "Kenneth",
      last_name: "Phang",
      age: 40
    },
    {
      first_name: "Kenneth2",
      last_name: "Phang",
      age: 40
    },
    {
      first_name: "Kenneth3",
      last_name: "Phang",
      age: 40
    }
  ]
}
