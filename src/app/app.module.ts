import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { HelloworldComponent } from './helloworld/helloworld.component';
import { Helloworld2Component } from './helloworld2/helloworld2.component';


@NgModule({
  declarations: [
    AppComponent,
    HelloworldComponent,
    Helloworld2Component
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
